//All the js code is here baby

//just some console stuff to begin with
console.log("Hi, I'm a console message. Thank God, you found me!");

//variables
/*
    usually the var keyword is used to create variables but starting with ES6, the let keyword has replaced it because var kept shitting the bed all the time.
*/
let whatAmI = "Aha, a variable!";

//constants
//constants are values that don't change over time.
const imSoStubborn = "Because I'm a constant and I'll sing it from the rooftops!";

//primitive data types
/*
    primitive types AKA value types. here we have, strings, numbers, boolean, undefined and null
*/

let booya = "I'm a string.";
let weAre = 5; //all numbers are just numbers, no int or float or anything else. just numbers.
let weWon = true;
let wtf; //it's not initialized, so it's undefined. though you can do it like this, wtf = undefined and it would mean the same. HAAHAA!!! And btw, the type is also undefined.
let imSoLite = null; //don;t judge me because I'm an object bruh! (null is an object)

//actually, type of a variable changes over time depending on the value assigned to it. that's one reason js is awesome.
let typeCheck = typeof(weAre);
console.log(typeCheck);

//operators
// =, -, /, *, %, &&, ||, ++, --



//reference types
/*
    we have objects, arrays and functions
*/

/* the curly braces { & } are called object literals
   objects can be accessed using either the dot notation or the bracket notation 
*/

let person = {
    name : 'Surya',
    age : 21,
    title : 'badass'
};

person.name = 'Deepak Surya'; //accessing properties of an object using the dot notation

console.log(person); //accessing the entire object

person['name'] = 'Deepak Surya .S'; //accessing properties of an object using the bracket notation

console.log(person.name);
console.log(person['title']);

//arrays
/*
    arrays can hold many types of value at once, hence they are dynamic in nature
    FYI arrays are objects too
    try typeof(arrayname)
    therefore dot and bracket notations are possible. of course the dot notations are used to access methods of arrays and bracket notation is used to access the elements in the array
*/
let cookies = ['Chocolate chip', 'Brittania', 'Hide and Seek'];
console.log(cookies);
console.log(cookies[1]);
//adding elements to the array
cookies[2] = 'Milk Bikies'; //of course this will replace hide and seek
console.log(cookies);
console.log(cookies.length);

//conditional statements
let checkThis = 7;
//if and else
console.log("if and else statements");
if (checkThis == 7) {
    console.log("We're even.");
} else {
    console.log("Nope, not even.");
}

//loops
//while
console.log("while loop");
while (checkThis > 0) {
    checkThis--;
    console.log(checkThis);
}
//do...while
console.log("do-while loop");
do{
    checkThis++;
    console.log(checkThis);
} while (checkThis < 7);
//for
console.log("for loop");
for (checkThis = 0; checkThis <= 7; checkThis++) {
    console.log(checkThis);
}




//alright guys, grab your coats we're going to functions. lol!

function greet(bff) { //the function has bff as a paramter
    console.log('Hey, ' + bff  + '!');
    console.log(typeof(greet));
}

greet('Donna'); //donna is passed as a argument to the function greet