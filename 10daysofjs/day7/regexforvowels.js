'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}


function regexVar() {
    /*
     * Declare a RegExp object variable named 're'
     * It must match a string that starts and ends with the same vowel (i.e., {a, e, i, o, u})
     */


    let re = RegExp(/^([aeiou]).*\1$/);
    
    /*
     *	^ - matches in the beginning
     *	[aeiou] - matches these characters
     *	. - matches any character except line terminators
     *	* - is a quantifier, matches between zero and unlimited times
     *	\1 - matches the same text as most recently matched by the 1st capturing group
     *	$ - assertion point at the end of the line
     */
    
    /*
     * Do not remove the return statement
     */
    return re;
}
