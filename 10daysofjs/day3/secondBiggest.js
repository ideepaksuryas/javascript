'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

/**
*   Return the second largest number in the array.
*   @param {Number[]} nums - An array of numbers.
*   @return {Number} The second largest number in the array.
**/
function getSecondLargest(nums) {
    // Complete the function
    var biggest = 0, next_biggest = 0;
    for (var i = 0; i < nums.length; i++) {
        var num = +nums[i]; //making sure everything in the array is a number
        if (num > biggest) {
            next_biggest = biggest;
            biggest = num;
        } else if (num < biggest && num > next_biggest) {
            next_biggest = num;
        }
    }
    return next_biggest;
}


function main() {
    const n = +(readLine());
    const nums = readLine().split(' ').map(Number);
    
    console.log(getSecondLargest(nums));
}
