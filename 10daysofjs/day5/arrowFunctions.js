/*
 * Modify and return the array so that all even elements are doubled and all odd elements are tripled.
 * 
 * Parameter(s):
 * nums: An array of numbers.
 */
function modifyArray(nums) {
    var array = nums.map(value => (value % 2 == 0) ? (value * 2) : (value * 3));
    return array;
}
